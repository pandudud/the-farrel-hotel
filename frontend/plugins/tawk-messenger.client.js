import Vue from 'vue';
import TawkMessengerVue from '@tawk.to/tawk-messenger-vue-2';

export default function () {
  Vue.use(TawkMessengerVue, {
    propertyId: '62b5e8fd7b967b1179965e8b',
    widgetId: '1g6b9c915'
  });
}