import Vue from 'vue';
import startApp from '~/mixins/start';

if (!Vue.__startApp__) {
  Vue.__startApp__ = true;
  Vue.mixin({
    mixins: [startApp],
  })
}