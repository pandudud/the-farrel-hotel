import Vue from 'vue';
var numeral = require('numeral');

if (numeral.locales['usa'] === undefined) {
  numeral.register('locale', 'usa', {
    delimiters: {
      thousands: ',',
      decimal: '.'
    },
    abbreviations: {
      thousand: 'k',
      million: 'm',
      billion: 'b',
      trillion: 't'
    },
    currency: {
      symbol: '$'
    }
  });
}

if (numeral.locales['ina'] === undefined) {
  numeral.register('locale', 'ina', {
    delimiters: {
      thousands: '.',
      decimal: ','
    },
    abbreviations: {
      thousand: 'rb',
      million: 'jt',
      billion: 'bl',
      trillion: 'tr'
    },
    currency: {
      symbol: 'IDR'
    }
  });
}

numeral.locale('ina');
numeral.defaultFormat('$ 0a');

export default ({app}, inject) => {
  inject('numeral', numeral);
}