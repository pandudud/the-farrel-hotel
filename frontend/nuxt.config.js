export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'The Farrel Hotel',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: '/js/jquery.js', type: "text/javascript", body: true, ssr: false },
      { src: '/js/plugins.js', type: "text/javascript", body: true, ssr: false },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/plugins.css',
    '~/assets/css/style.css',
    '~/assets/css/fonts.css',
    '~/assets/homepages/hotel/css/hotel-style.css',
    '~/assets/css/custom.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/tawk-messenger.client.js', mode: 'client' },
    { src: '~/plugins/numeral.js'},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
    '@nuxtjs/moment',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxt/image',
    '@nuxtjs/i18n',
    '@nuxt/http',
    ['nuxt-gmaps', { key: 'AIzaSyAmVApEVfjwgNgi5_nOqFUjifzx7AEvU5w' }]
  ],

  styleResources: {
    scss: [
      './assets/scss/*.scss',
    ],
  },

  axios: {
    baseURL: 'https://app.thefarrelhotel.com/api'
  },

  http: {
    baseURL: 'https://app.thefarrelhotel.com/api'
  },

  i18n: {
    locales: ['en', 'id'],
    defaultLocale: 'id',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: require('./locales/en.json'),
        id: require('./locales/id.json'),
      }
    }
  },

  moment: {
    defaultLocale: 'id',
    defaultTimezone: 'Asia/Jakarta',
    locales: ['id']
  },

  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.BASE_URL
    },
    http: {
      browserBaseURL: process.env.BASE_URL
    }
  },

  privateRuntimeConfig: {
    axios: {
      baseURL: process.env.BASE_URL
    },
    http: {
      baseURL: process.env.BASE_URL
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      compact: true,
    },
    postcss: false,
  },

  server: {
    host: '0.0.0.0',
    port: 29537
  },

  target: 'static',
}
