export default {
  fetchSurroundingsData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/arounds",
        params: payload,
      })
      .then((res) => {
        commit("setSurroundingsData", res.data.data);
        commit("setSurroundingsPagination", res.data.pagination);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  async http_fetchSurroundingsData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/arounds?', {searchParams: payload});
    if(data) {
      commit("setSurroundingsData", data.data);
      commit("setSurroundingsPagination", data.pagination);
    }
  },
}