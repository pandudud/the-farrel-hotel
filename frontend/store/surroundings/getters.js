export default {
  getSurroundingsData(state) {
    return state.surroundings_data
  },
  getSurroundingsPagination(state) {
    return state.surroundings_pagination
  },
}