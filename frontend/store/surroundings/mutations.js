export default {
  setSurroundingsData(state, payload) {
    state.surroundings_data = payload
  },
  setSurroundingsPagination(state, payload) {
    state.surroundings_pagination = payload
  },
}
