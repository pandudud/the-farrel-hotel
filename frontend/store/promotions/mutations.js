export default {
  setPromotionsData(state, payload) {
    state.promotions_data = payload
  },
  setPromotionsPagination(state, payload) {
    state.promotions_pagination = payload
  },
  setPromotionData(state, payload) {
    state.promotion_data = payload
  },
}
