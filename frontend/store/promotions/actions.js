export default {
  fetchPromotionsData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/promotions",
        params: payload,
      })
      .then((res) => {
        commit("setPromotionsData", res.data.data);
        commit("setPromotionsPagination", res.data.pagination);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  fetchPromotionData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/promotion/" + payload,
      })
      .then((res) => {
        commit("setPromotionData", res.data);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  async http_fetchPromotionsData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/promotions?', {searchParams: payload});
    if(data) {
      commit("setPromotionsData", data.data);
      commit("setPromotionsPagination", data.pagination);
    }
  },
  async http_fetchPromotionData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/promotion/' + payload);
    if(data) {
      commit("setPromotionData", data);
    }
  },
}