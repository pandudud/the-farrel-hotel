export default {
  getPromotionsData(state) {
    return state.promotions_data
  },
  getPromotionsPagination(state) {
    return state.promotions_pagination
  },
  getPromotionData(state) {
    return state.promotion_data
  },
}