export default {
  setGalleriesData(state, payload) {
    state.galleries_data = payload
  },
  setGalleriesPagination(state, payload) {
    state.galleries_pagination = payload
  },
}
