export default {
  getGalleriesData(state) {
    return state.galleries_data
  },
  getGalleriesPagination(state) {
    return state.galleries_pagination
  },
}