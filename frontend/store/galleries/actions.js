export default {
  fetchGalleriesData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/galleries",
        params: payload,
      })
      .then((res) => {
        commit("setGalleriesData", res.data.data);
        commit("setGalleriesPagination", res.data.pagination);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  async http_fetchGalleriesData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/galleries?', {searchParams: payload});
    if(data) {
      commit("setGalleriesData", data.data);
      commit("setGalleriesPagination", data.pagination);
    }
  },
}