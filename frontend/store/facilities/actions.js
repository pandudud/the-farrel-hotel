export default {
  fetchFacilitiesData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/facilities",
        params: payload,
      })
      .then((res) => {
        commit("setFacilitiesData", res.data.data);
        commit("setFacilitiesPagination", res.data.pagination);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  async http_fetchFacilitiesData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/facilities?', {searchParams: payload});
    if(data) {
      commit("setFacilitiesData", data.data);
      commit("setFacilitiesPagination", data.pagination);
    }
  },
  async http_fetchFacilityData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/facility/' + payload);
    if(data) {
      commit("setFacilityData", data);
    }
  },
}