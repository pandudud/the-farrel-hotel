export default {
  getFacilitiesData(state) {
    return state.facilities_data
  },
  getFacilitiesPagination(state) {
    return state.facilities_pagination
  },
  getFacilityData(state) {
    return state.facility_data
  },
}