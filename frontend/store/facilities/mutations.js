export default {
  setFacilitiesData(state, payload) {
    state.facilities_data = payload
  },
  setFacilitiesPagination(state, payload) {
    state.facilities_pagination = payload
  },
  setFacilityData(state, payload) {
    state.facility_data = payload
  },
}
