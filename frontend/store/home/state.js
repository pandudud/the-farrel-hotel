export default () => ({
  home_banners: [],
  home_facilities: [],
  home_rooms: [],
  home_promos: [],
  home_social_media: {},
  home_contact: {},
})