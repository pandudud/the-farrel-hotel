export default {
  setHomeBanners(state, payload) {
    state.home_banners = payload
  },
  setHomeFacilities(state, payload) {
    state.home_facilities = payload
  },
  setHomeRooms(state, payload) {
    state.home_rooms = payload
  },
  setHomePromos(state, payload) {
    state.home_promos = payload
  },
  setHomeSocialMedia(state, payload) {
    state.home_social_media = payload
  },
  setHomeContact(state, payload) {
    state.home_contact = payload
  },
}
