export default {
  fetchBanner({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/home/banner",
      }).then((res) => {
        commit("setHomeBanners", res.data);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  fetchFacility({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/home/facility",
      }).then((res) => {
        commit("setHomeFacilities", res.data);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  fetchRoom({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/home/room",
      }).then((res) => {
        commit("setHomeRooms", res.data);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  fetchSocialMedia({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/home/social-media",
      }).then((res) => {
        commit("setHomeSocialMedia", res.data);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  fetchContact({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/home/contact",
      }).then((res) => {
        commit("setHomeContact", res.data);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  async http_fetchBanner({ commit }) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/home/banner');
    if(data) {
      commit("setHomeBanners", data);
    }
  },
  async http_fetchFacility({ commit }) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/home/facility');
    if(data) {
      commit("setHomeFacilities", data);
    }
  },
  async http_fetchRoom({ commit }) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/home/room');
    if(data) {
      commit("setHomeRooms", data);
    }
  },
  async http_fetchSocialMedia({ commit }) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/home/social-media');
    if(data) {
      commit("setHomeSocialMedia", data);
    }
  },
  async http_fetchContact({ commit }) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/home/contact');
    if(data) {
      commit("setHomeContact", data);
    }
  },
}