export default {
  getHomeBanners(state) {
    return state.home_banners
  },
  getHomeFacilities(state) {
    return state.home_facilities
  },
  getHomeRooms(state) {
    return state.home_rooms
  },
  getHomePromos(state) {
    return state.home_promos
  },
  getHomeSocialMedia(state) {
    return state.home_social_media
  },
  getHomeContact(state) {
    return state.home_contact
  },
}