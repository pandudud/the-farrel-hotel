export default {
  fetchEventsData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/events",
        params: payload,
      })
      .then((res) => {
        commit("setEventsData", res.data.data);
        commit("setEventsPagination", res.data.pagination);
        resolve(res.data);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  async http_fetchEventsData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/events?', {searchParams: payload});
    if(data) {
      commit("setEventsData", data.data);
      commit("setEventsPagination", data.pagination);
    }
  },
}