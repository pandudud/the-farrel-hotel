export default {
  setEventsData(state, payload) {
    state.events_data = payload
  },
  setEventsPagination(state, payload) {
    state.events_pagination = payload
  },
}
