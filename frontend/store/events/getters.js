export default {
  getEventsData(state) {
    return state.events_data
  },
  getEventsPagination(state) {
    return state.events_pagination
  },
}