export default {
  fetchRoomsData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/rooms",
        params: payload,
      })
      .then((res) => {
        commit("setRoomsData", res.data.data);
        commit("setRoomsPagination", res.data.pagination);
        resolve(res.data);
      }).catch((err) => {
        this.app.context.error({ statusCode: 404 });
        reject(err);
      });
    });
  },
  fetchRoomData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios({
        method: "get",
        url: this.$axios.defaults.baseURL + "/room/" + payload,
      })
      .then((res) => {
        commit("setRoomData", res.data);
        resolve(res.data);
      }).catch((err) => {
        this.app.context.error({ statusCode: 404 });
        reject(err);
      });
    });
  },
  async http_fetchRoomsData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/rooms?', {searchParams: payload});
    if(data) {
      commit("setRoomsData", data.data);
      commit("setRoomsPagination", data.pagination);
    }
  },
  async http_fetchRoomData({ commit }, payload) {
    const data = await this.$http.$get(this.$http.getBaseURL() + '/room/' + payload);
    if(data) {
      commit("setRoomData", data);
    }
  },
}