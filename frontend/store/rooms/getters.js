export default {
  getRoomsData(state) {
    return state.rooms_data
  },
  getRoomsPagination(state) {
    return state.rooms_pagination
  },
  getRoomData(state) {
    return state.room_data
  },
}