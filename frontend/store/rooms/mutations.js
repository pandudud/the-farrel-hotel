export default {
  setRoomsData(state, payload) {
    state.rooms_data = payload
  },
  setRoomsPagination(state, payload) {
    state.rooms_pagination = payload
  },
  setRoomData(state, payload) {
    state.room_data = payload
  },
}
