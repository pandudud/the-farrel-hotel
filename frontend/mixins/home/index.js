export default {
  data() {
    return {
      forceUpdate: 0,
    }
  },
  computed: {
    banners: function() {
      return this.$store.state.home.home_banners;
    },
    facilities: function() {
      return this.$store.state.home.home_facilities;
    },
    rooms: function() {
      return this.$store.state.home.home_rooms;
    },
  },
  async mounted() {
    await this.$store.dispatch('home/http_fetchBanner');
    await this.$store.dispatch('home/http_fetchFacility');
    await this.$store.dispatch('home/http_fetchRoom');
  }
}