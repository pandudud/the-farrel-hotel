import { mapGetters } from "vuex";
export default {
  data() {
    return {
      loading: false,
      rooms_data: [],
      rooms_pagination: {},
    }
  },
  computed: {
    query: function() {
      return this.$nuxt.$route.query;
    },
    ...mapGetters({
      getRoomsData: "rooms/getRoomsData",
      getRoomsPagination: "rooms/getRoomsPagination",
    }),
  },
  watch: {
    query: function() {
      this.fetchRoomsData();
    },
  },
  methods: {
    fetchRoomsData: async function() {
      this.loading = true;
      await this.$store.dispatch("rooms/http_fetchRoomsData", this.query);
      this.rooms_data = this.getRoomsData;
      this.rooms_pagination = this.getRoomsPagination;
      this.loading = false;
    },
  },
  mounted: function() {
    this.fetchRoomsData();
  }
}