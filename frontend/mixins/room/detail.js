import { mapGetters } from "vuex";
export default {
  fetchOnServer: false,
  async asyncData({ params }) {
    const slug = params.slug;
    return { slug };
  },
  async fetch() {
    await this.$store.dispatch("rooms/http_fetchRoomData", this.slug);
    this.room_data = this.getRoomData;
  },
  watch: {
    slug() {
      this.$fetch();
    },
  },
  data() {
    return {
      room_data: {},
    }
  },
  computed: {
    c_room_name: function() {
      if(this.$i18n.locale == 'id') {
        return this.room_data.room_name;
      }
      return this.room_data.room_name_eng;
    },
    c_room_desc: function() {
      if(this.$i18n.locale == 'id') {
        return this.room_data.room_description;
      }
      return this.room_data.room_description_eng;
    },
    ...mapGetters({
      getRoomData: "rooms/getRoomData",
    }),
  }
}