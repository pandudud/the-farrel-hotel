import { mapGetters } from "vuex";
export default {
  data() {
    return {
      loading: true,
      events_data: [],
      events_pagination: {},
    }
  },
  computed: {
    query: function() {
      return this.$nuxt.$route.query;
    },
    ...mapGetters({
      getEventsData: "events/getEventsData",
      getEventsPagination: "events/getEventsPagination",
    }),
  },
  watch: {
    query: function() {
      this.fetchEventsData();
    },
  },
  methods: {
    fetchEventsData: async function() {
      this.loading = true;
      await this.$store.dispatch("events/http_fetchEventsData", this.query);
      this.events_data = this.getEventsData;
      this.events_pagination = this.getEventsPagination;
      this.loading = false;
    },
  },
  mounted: function() {
    this.fetchEventsData();
  }
}