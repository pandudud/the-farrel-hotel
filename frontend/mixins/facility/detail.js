import { mapGetters } from "vuex";
export default {
  fetchOnServer: false,
  async asyncData({ params }) {
    const slug = params.slug;
    return { slug };
  },
  async fetch() {
    await this.$store.dispatch("facilities/http_fetchFacilityData", this.slug);
    this.facility_data = this.getFacilityData;
  },
  watch: {
    slug() {
      this.$fetch();
    },
  },
  data() {
    return {
      facility_data: {},
    }
  },
  computed: {
    c_facility_name: function() {
      if(this.$i18n.locale == 'id') {
        return this.facility_data.facility_name;
      }
      return this.facility_data.facility_name_eng;
    },
    c_facility_desc: function() {
      if(this.$i18n.locale == 'id') {
        return this.facility_data.facility_description;
      }
      return this.facility_data.facility_description_eng;
    },
    c_facility_detail: function() {
      if(this.$i18n.locale == 'id') {
        return this.facility_data.facility_detail;
      }
      return this.facility_data.facility_detail_eng;
    },
    ...mapGetters({
      getFacilityData: "facilities/getFacilityData",
    }),
  }
}