import { mapGetters } from "vuex";
export default {
  data() {
    return {
      loading: true,
      facilities_data: [],
      facilities_pagination: {},
    }
  },
  computed: {
    query: function() {
      return this.$nuxt.$route.query;
    },
    ...mapGetters({
      getFacilitiesData: "facilities/getFacilitiesData",
      getFacilitiesPagination: "facilities/getFacilitiesPagination",
    }),
  },
  watch: {
    query: function() {
      this.fetchFacilitiesData();
    },
  },
  methods: {
    fetchFacilitiesData: async function() {
      this.loading = true;
      await this.$store.dispatch("facilities/http_fetchFacilitiesData", this.query);
      this.facilities_data = this.getFacilitiesData;
      this.facilities_pagination = this.getFacilitiesPagination;
      this.loading = false;
    },
  },
  mounted: function() {
    this.fetchFacilitiesData();
  }
}