import { mapGetters } from "vuex";
export default {
  data() {
    return {
      loading: true,
      surroundings_data: [],
      surroundings_pagination: {},
    }
  },
  computed: {
    query: function() {
      return this.$nuxt.$route.query;
    },
    ...mapGetters({
      getSurroundingsData: "surroundings/getSurroundingsData",
      getSurroundingsPagination: "surroundings/getSurroundingsPagination",
    }),
  },
  watch: {
    query: function() {
      this.fetchSurroundingsData();
    },
  },
  methods: {
    fetchSurroundingsData: async function() {
      this.loading = true;
      await this.$store.dispatch("surroundings/http_fetchSurroundingsData", this.query);
      this.surroundings_data = this.getSurroundingsData;
      this.surroundings_pagination = this.getSurroundingsPagination;
      this.loading = false;
    },
  },
  mounted: function() {
    this.fetchSurroundingsData();
  }
}