import { mapGetters } from "vuex";
export default {
  data() {
    return {
      loading: true,
      promotions_data: [],
      promotions_pagination: {},
    }
  },
  computed: {
    query: function() {
      return this.$nuxt.$route.query;
    },
    ...mapGetters({
      getPromotionsData: "promotions/getPromotionsData",
      getPromotionsPagination: "promotions/getPromotionsPagination",
    }),
  },
  watch: {
    query: function() {
      this.fetchPromotionsData();
    },
  },
  methods: {
    fetchPromotionsData: async function() {
      this.loading = true;
      await this.$store.dispatch("promotions/http_fetchPromotionsData", this.query);
      this.promotions_data = this.getPromotionsData;
      this.promotions_pagination = this.getPromotionsPagination;
      this.loading = false;
    },
  },
  mounted: function() {
    this.fetchPromotionsData();
  }
}