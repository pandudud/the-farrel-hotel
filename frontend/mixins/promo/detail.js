import { mapGetters } from "vuex";
export default {
  fetchOnServer: false,
  async asyncData({ params }) {
    const slug = params.slug;
    return { slug };
  },
  async fetch() {
    await this.$store.dispatch("promotions/http_fetchPromotionData", this.slug);
    this.promotion_data = this.getPromotionData;
  },
  watch: {
    slug() {
      this.$fetch();
    },
  },
  data() {
    return {
      promotion_data: {},
    }
  },
  computed: {
    c_promotion_name: function() {
      if(this.$i18n.locale == 'id') {
        return this.promotion_data.promotion_name;
      }
      return this.promotion_data.promotion_name_eng;
    },
    c_promotion_desc: function() {
      if(this.$i18n.locale == 'id') {
        return this.promotion_data.promotion_description;
      }
      return this.promotion_data.promotion_description_eng;
    },
    ...mapGetters({
      getPromotionData: "promotions/getPromotionData",
    }),
  }
}