import { mapGetters } from "vuex";
export default {
  data() {
    return {
      loading: true,
      galleries_data: [],
      galleries_pagination: {},
    }
  },
  computed: {
    query: function() {
      return this.$nuxt.$route.query;
    },
    ...mapGetters({
      getGalleriesData: "galleries/getGalleriesData",
      getGalleriesPagination: "galleries/getGalleriesPagination",
    }),
  },
  watch: {
    query: function() {
      this.fetchGalleriesData();
    },
  },
  methods: {
    fetchGalleriesData: async function() {
      this.loading = true;
      await this.$store.dispatch("galleries/http_fetchGalleriesData", this.query);
      this.galleries_data = this.getGalleriesData;
      this.galleries_pagination = this.getGalleriesPagination;
      this.loading = false;
    },
  },
  mounted: function() {
    this.fetchGalleriesData();
  }
}