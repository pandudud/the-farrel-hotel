export const menuItems = [
  {
    id: 1,
    label: "menuitems.room.text",
    link: "rooms"
  },
  {
    id: 2,
    label: "menuitems.facility.text",
    link: "facilities"
  },
  {
    id: 3,
    label: "menuitems.event.text",
    link: "events"
  },
  {
    id: 4,
    label: "menuitems.promo.text",
    link: "promotions"
  },
  {
    id: 5,
    label: "menuitems.gallery.text",
    link: "galleries"
  },
  {
    id: 6,
    label: "menuitems.surrounding.text",
    link: "surroundings"
  },
  // {
  //   id: 7,
  //   label: "menuitems.about_us.text",
  //   link: "about-us"
  // },
  {
    id: 8,
    label: "menuitems.contact_us.text",
    link: "contact-us"
  },
];